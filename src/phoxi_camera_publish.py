#!/usr/bin/env python
import rospy
from phoxi_camera.srv import *
from std_srvs.srv import *

if __name__ == "__main__":
    rospy.init_node('phoxi_camera_example', anonymous=True)
    rospy.wait_for_service('phoxi_camera/get_device_list')
    get_device_list = rospy.ServiceProxy('phoxi_camera/get_device_list', GetDeviceList)
    resp1 = get_device_list()
    print "devices", resp1.out
    name = "2018-03-025-LC3"
    res_connect = rospy.ServiceProxy('phoxi_camera/connect_camera', ConnectCamera)(name)
    print "connect to", name, res_connect
    while not rospy.is_shutdown():      
        try:         
            if res_connect.success:
                res_get_fram = rospy.ServiceProxy('phoxi_camera/get_frame', GetFrame)(-1)
                print "get_frame", res_get_fram
            else:
                print "can't connect"
        except rospy.ServiceException, e:
            print "Service call failed: %s" % e
     
